class Score {
    constructor(ps, cs, bt) {
        var s1 = ps;
        var s2 = cs;
        this.pScore = document.getElementById(s1);
        this.cScore = document.getElementById(s2);
        this.buttons = document.querySelectorAll(bt);

        this.rockIcon = "rock";
        this.paperIcon = "paper";
        this.scissorsIcon = "scissors";

        this.randomClasses = ["rock", "paper", "scissors"];
    }
}

class Text extends Score {
    constructor(ps, cs, bt, tx, cSI) {
        super(ps, cs, bt)
        this.text2 = document.getElementById(tx);
        var pcSI = () => {
            return cSI * 1;
        }
        this.pcS = pcSI();
    }
}


const finalScore = new Text('p-score', 'c-score', '.user button', 'hasil', 1);

let computerShowIcon = '';
let computerScore = finalScore.pcS;
let playerScore = finalScore.pcS;

const tie = (v) => {
    console.log(v);
    v.style.backgroundColor = '#b7d4d7';
    finalScore.text2.innerHTML = "DRAW";
    finalScore.text2.style.color = 'white';
    finalScore.text2.style.backgroundColor = '#8b5097';
    finalScore.text2.style.fontSize = '2.5rem';
    finalScore.text2.style.transform = 'rotate(-25deg)';
}

const win = (v) => {
    console.log(v);
    v.style.backgroundColor = '#b7d4d7';
    finalScore.text2.innerHTML = 'PLAYER 1 WIN';
    finalScore.text2.style.color = 'white';
    finalScore.text2.style.backgroundColor = '#9ed163';
    finalScore.text2.style.fontSize = '2.5rem';
    finalScore.text2.style.transform = 'rotate(-25deg)';
}

const lose = (v) => {
    console.log(v)
    v.style.backgroundColor = '#b7d4d7';
    finalScore.text2.innerHTML = 'COM WIN';
    finalScore.text2.style.color = 'white';
    finalScore.text2.style.backgroundColor = '#9ed163';
    finalScore.text2.style.fontSize = '2.5rem';
    finalScore.text2.style.transform = 'rotate(-25deg)';
}

const disabled = () => {
    document.getElementById(`player-rock`).disabled = true;
    document.getElementById(`player-paper`).disabled = true;
    document.getElementById(`player-scissors`).disabled = true;
}

const style = async (v) => {
    console.log(v)
    if (v === 'rock') {
        document.querySelector(`.user-paper`).style.backgroundColor = '';
        document.querySelector(`.user-scissors`).style.backgroundColor = '';
        disabled();
    } else if (v === 'paper') {
        document.querySelector(`.user-rock`).style.backgroundColor = '';
        document.querySelector(`.user-scissors`).style.backgroundColor = '';
        disabled();
    } else {
        document.querySelector(`.user-rock`).style.backgroundColor = '';
        document.querySelector(`.user-paper`).style.backgroundColor = '';
        disabled();
    }
    const color = document.querySelector(`.user-${v}`);
    color.style.backgroundColor = '#b7d4d7';
}

const refresh = () => {
    if (parseInt(finalScore.pScore.innerHTML) + parseInt(finalScore.cScore.innerHTML) === 5) {
        finalScore.pScore.innerHTML = '0';
        finalScore.cScore.innerHTML = '0';
        playerScore = 1;
        computerScore = 1;
    }
    finalScore.text2.style = '';
    finalScore.text2.innerHTML = 'VS';
    document.getElementById(`player-rock`).disabled = false;
    document.getElementById(`player-paper`).disabled = false;
    document.getElementById(`player-scissors`).disabled = false;
    document.querySelector(`.user-rock`).style.backgroundColor = '';
    document.querySelector(`.user-paper`).style.backgroundColor = '';
    document.querySelector(`.user-scissors`).style.backgroundColor = '';
    document.querySelector(`.com-rock`).style.backgroundColor = '';
    document.querySelector(`.com-paper`).style.backgroundColor = '';
    document.querySelector(`.com-scissors`).style.backgroundColor = '';
}


const game = () => {
    finalScore.buttons.forEach(btn => {
        btn.addEventListener('click', (e) => {
            let clickedBtn = e.target.className;
            let randomNum = Math.floor(Math.random() * finalScore.randomClasses.length);
            computerShowIcon = finalScore.randomClasses[randomNum];
            style(clickedBtn);
            const p2 = document.querySelector(`.com-${computerShowIcon}`);
            console.log(computerShowIcon);
            console.log(p2);
            if (clickedBtn === computerShowIcon) {
                finalScore.pScore.innerHTML = finalScore.pScore.innerHTML;
                finalScore.cScore.innerHTML = finalScore.cScore.innerHTML;
                tie(p2);
            }

            else if (clickedBtn === finalScore.rockIcon && computerShowIcon === finalScore.scissorsIcon) {
                finalScore.pScore.innerHTML = playerScore;
                playerScore++;
                win(p2);
            } else if (clickedBtn === finalScore.rockIcon && computerShowIcon === finalScore.paperIcon) {
                finalScore.cScore.innerHTML = computerScore;
                computerScore++;
                lose(p2);
            } else if (clickedBtn === finalScore.paperIcon && computerShowIcon === finalScore.scissorsIcon) {
                finalScore.cScore.innerHTML = computerScore;
                computerScore++;
                lose(p2);
            } else if (clickedBtn === finalScore.paperIcon && computerShowIcon === finalScore.rockIcon) {
                finalScore.pScore.innerHTML = playerScore;
                playerScore++;
                win(p2);
            } else if (clickedBtn === finalScore.scissorsIcon && computerShowIcon === finalScore.rockIcon) {
                finalScore.cScore.innerHTML = computerScore;
                computerScore++;
                lose(p2);
            } else if (clickedBtn === finalScore.scissorsIcon && computerShowIcon === finalScore.paperIcon) {
                finalScore.pScore.innerHTML = playerScore;
                playerScore++;
                win(p2);
            }

            if (parseInt(finalScore.pScore.innerHTML) + parseInt(finalScore.cScore.innerHTML) === 5) {
                parseInt(finalScore.pScore.innerHTML) > parseInt(finalScore.cScore.innerHTML) ? alert("PLAYER WIN") : alert("COM WIN");
            }
        });
    });
}

game();
